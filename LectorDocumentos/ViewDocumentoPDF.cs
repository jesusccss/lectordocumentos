﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LectorDocumentos
{
    public partial class ViewDocumentoPDF : Form
    {
        //documentViewer1
        private string _ruta;
        private string _nombre;
        
        public ViewDocumentoPDF(string dir,string nom)
        {
            InitializeComponent();
            this._ruta = dir;
            this._nombre = nom;
            this.AbrirDocumento();

        }// end ViewDocumentoPDF

        private void ViewDocumentoPDF_Load(object sender, EventArgs e){
            pdfViewer01.Dock = DockStyle.Fill;
        }// end ViewDocumentoPDF_Load


        public void AbrirDocumento() {
            this.Text = this._nombre;
            pdfViewer01.LoadDocument( this._ruta );
            pdfViewer01.ContextMenu = null;

            



        }// end AbrirDocumento

        private void pdfViewer01_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) {
                //SendKeys.Send("{ESC}");
                //MessageBox.Show("Menu Deshabilitado", "", MessageBoxButtons.OK);
                MessageBox.Show("Menu Deshabilitado", "Lector de documentos", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }// end pdfViewer01_MouseDown

        private void pdfViewer01_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //SendKeys.Send("{ESC}");
                MessageBox.Show("Menu Deshabilitado", "Lector de documentos", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }// end pdfViewer01_MouseUp





    }// end class
}// end namespace
