﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LectorDocumentos
{
    public partial class ArbolDirectorio : Form
    {

        private ViewDocumentoPDF nuevo;
        public enum Orden { Ascendente = 0, Descendente = 1 };
        private ListViewColumnSorter lvwColumnSorter;// columna para ordenador

        public ArbolDirectorio()
        {
            InitializeComponent();
            CrearArbol();
            colocar();
            
            // Create an instance of a ListView column sorter and assign it // to the ListView control.
            lvwColumnSorter = new ListViewColumnSorter();
            this.ListaDeArchivos.ListViewItemSorter = lvwColumnSorter;
            OrdenarColumna(Orden.Ascendente);// ordeno la columna


        }// end ArbolDirectorio

        

        private void Form1_Load(object sender, EventArgs e)
        {

        }// end load

        public string _raiz = @"\\10.130.233.25\compartida\DocumentosCertificacion";
        private string referenciaLetras = "abcdefghijklmnopqrstuvwxyz1234567890 +-*/._:;,<>()°|!";

        



        private void CrearArbol(){
            Arbol.Nodes.Clear();// elimina nodos
            TreeNode rootNode;

            //            DirectoryInfo info = new DirectoryInfo(@"../..");
            DirectoryInfo info = new DirectoryInfo( _raiz );
            if (info.Exists){
                rootNode = new TreeNode(info.Name);
                rootNode.Tag = info;
                GetDirectories(info.GetDirectories(), rootNode);
                Arbol.Nodes.Add(rootNode);
            }
        }// end PopulateTreeView

        private void GetDirectories(DirectoryInfo[] subDirs,TreeNode nodeToAddTo){
            TreeNode aNode;
            DirectoryInfo[] subSubDirs;
            foreach (DirectoryInfo subDir in subDirs){
                aNode = new TreeNode(subDir.Name, 0, 0);
                aNode.Tag = subDir;
                aNode.ImageKey = "folder";
                subSubDirs = subDir.GetDirectories();
                if (subSubDirs.Length != 0)
                {
                    GetDirectories(subSubDirs, aNode);
                }
                nodeToAddTo.Nodes.Add(aNode);
            }
        }// end GetDirectories

        public string[] burbuja(ListBox arr) {



            return (new string[] { "",""}); // falta por definir

        }// end burbuja


        private int letra(char c) {



                return 1;

        }// end valorletra



        void treeView1_NodeMouseClick(object sender,TreeNodeMouseClickEventArgs e){

            TreeNode newSelected = e.Node;
            ListaDeArchivos.Items.Clear();
            DirectoryInfo nodeDirInfo = (DirectoryInfo)newSelected.Tag;
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;


            //foreach (DirectoryInfo dir in nodeDirInfo.GetDirectories())
            //{
            //    item = new ListViewItem(dir.Name, 0);
            //    subItems = new ListViewItem.ListViewSubItem[]
            //              {new ListViewItem.ListViewSubItem(item, "Carpeta"),
            //       new ListViewItem.ListViewSubItem(item,
            //    dir.LastAccessTime.ToShortDateString())};
            //    item.SubItems.AddRange(subItems);
            //    listView1.Items.Add(item);
            //}



            string nombre = "", extencion = "", _revision = "";
            char punto = '.';
            ListBox listaDesorden = new ListBox();
            foreach (FileInfo file in nodeDirInfo.GetFiles()) {
                listaDesorden.Items.Add(file.Name);
            }// 

            for (int i = 0; i<listaDesorden.Items.Count ; i++) {
                


            }// ordenar lista



            foreach (FileInfo file in nodeDirInfo.GetFiles())
            {
                nombre = file.Name.Substring( 0, file.Name.Length-4);
                extencion = file.Name.Substring(file.Name.Length -3,3);


                if (file.Name.Contains("REV.0")) { _revision = "Rev.00"; }
                else if (file.Name.Contains("REV.1")) { _revision = "Rev.01"; }
                else if (file.Name.Contains("REV.2")) { _revision = "Rev.02"; }
                else if (file.Name.Contains("REV.3")) { _revision = "Rev.03"; }
                else if (file.Name.Contains("REV.4")) { _revision = "Rev.04"; }
                else if (file.Name.Contains("REV.5")) { _revision = "Rev.05"; }
                else if (file.Name.Contains("REV.6")) { _revision = "Rev.06"; }
                else if (file.Name.Contains("REV.7")) { _revision = "Rev.07"; }
                else if (file.Name.Contains("REV.8")) { _revision = "Rev.08"; }
                else if (file.Name.Contains("REV.9")) { _revision = "Rev.09"; }
                else if (file.Name.Contains("REV.10")) { _revision = "Rev.10"; }
                else if (file.Name.Contains("REV.11")) { _revision = "Rev.11"; }
                else if (file.Name.Contains("REV.12")) { _revision = "Rev.12"; }
                else if (file.Name.Contains("REV.13")) { _revision = "Rev.13"; }
                else if (file.Name.Contains("REV.14")) { _revision = "Rev.14"; }
                else if (file.Name.Contains("REV.15")) { _revision = "Rev.15"; }
                else { _revision = "Rev.XX"; }


                if (extencion == "pdf")
                    {// solo pdf
                        item = new ListViewItem(nombre, 1);// nombre
                        subItems = new ListViewItem.ListViewSubItem[]// agregar datos
                                  {    new ListViewItem.ListViewSubItem(item, _revision)// rev
                                  ,new ListViewItem.ListViewSubItem(item, extencion)// extencion
                                  ,new ListViewItem.ListViewSubItem(item,file.LastAccessTime.ToShortDateString())//ultima modificacion
                                  ,new ListViewItem.ListViewSubItem(item,file.DirectoryName.ToString().Replace(_raiz,"_raiz"))//directorio
                                  };

                        item.SubItems.AddRange(subItems);
                        ListaDeArchivos.Items.Add(item);

                    }// end if solo pdf
            }

            ListaDeArchivos.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }// end treeView1_NodeMouseClick

        private void Actualizar_Tick(object sender, EventArgs e)
        {
            //actualizar arbol de directorios
            CrearArbol();// actualizar arbol

        }// end Actualizar_Tick

        private void Form1_Resize(object sender, EventArgs e){

            colocar();

        }// end redimencionar formulario 

        public void colocar() {

            panelDividido.Top = 30;
            panelDividido.Height = this.Height - panelDividido.Top;

            panelDividido.Left = 0;
            panelDividido.Width = this.Width - 16;



        }// end colocar

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
//            //MessageBox.Show(e.ToString());


//            try
//            {
//                //MessageBox.Show(listView1.SelectedItems[0].ToString());






//                //if (listView1.SelectedItems.Count > 0) {

                 
//                //ListViewItem item = listView1.SelectedItems[0];
//                //string _ruta_completa = "";
//                //if (item != null)
//                //{

//                //    //_ruta_completa = item.SubItems[3].Text.ToString()
//                //    //                + "\\"
//                //    //                + item.SubItems[0].Text.ToString()
//                //    //                + "."
//                //    //                + item.SubItems[1].Text.ToString()
//                //    //                ;
//                //    //(new ViewDocumentoPDF()).AbrirDocumento( _ruta_completa );

//                //    MessageBox.Show(
//                //      //  "Valor 1: " + item.SubItems[0].Text.ToString()

//                //      //+ "Valor 2: " + item.SubItems[1].Text.ToString()
//                //      //                 "Ruta: " +
//                //      item.SubItems[3].Text.ToString()
//                //    + "\\"
//                //    + item.SubItems[0].Text.ToString()
//                //    + "."
//                //    + item.SubItems[1].Text.ToString()
//                //                            );
//                //    //EmpIDtextBox.Text = item.SubItems[0].Text;
//                //    //EmpNametextBox.Text = item.SubItems[1].Text;
//                //}







//                    //OpenFileDialog dlg = new OpenFileDialog();
//                    //dlg.Filter = "Archivos PDF (*.pdf) |*.pdf;";
//                    //dlg.FileName = String.Empty;

//                    //dlg.ShowDialog();
//                    //if (dlg.FileName != null){
//                    //}


//                    //System.Diagnostics.Process.Start("Acrobat.exe", dlg.FileName);
//                    //System.Diagnostics.Process.Start("Acrobat.exe", item.SubItems[0].Text.ToString());

//                    //If oOpenFileDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
//                    //TextBoxFile.Text = oOpenFileDialog.FileName
//                    //End If
//                //}


//            }
//            catch (Exception ex){

//                MessageBox.Show(ex.Message.ToString());
////                throw;
//            }

        }// end listView1_SelectedIndexChanged

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //if (listView1.SelectedItems.Count > 0)
            //{


            //    ListViewItem item = listView1.SelectedItems[0];
            //    string _ruta_completa = "";
            //    if (item != null)
            //    {

            //        //_ruta_completa = item.SubItems[3].Text.ToString()
            //        //                + "\\"
            //        //                + item.SubItems[0].Text.ToString()
            //        //                + "."
            //        //                + item.SubItems[1].Text.ToString()
            //        //                ;
            //        //(new ViewDocumentoPDF()).AbrirDocumento(_ruta_completa);

            //        MessageBox.Show(item.SubItems[3].Text.ToString()
            //                        + "\\"
            //                        + item.SubItems[0].Text.ToString()
            //                        + "."
            //                        + item.SubItems[1].Text.ToString()
            //                        );
            //        //EmpIDtextBox.Text = item.SubItems[0].Text;
            //        //EmpNametextBox.Text = item.SubItems[1].Text;
            //    }
            //}
        }// end 

        private void listView1_Click(object sender, EventArgs e)
        {

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                if (ListaDeArchivos.SelectedItems.Count > 0)
                {


                    ListViewItem item = ListaDeArchivos.SelectedItems[0];
                    string _ruta_completa = "", _nombre;
                    if (item != null)
                    {
                        

                        _ruta_completa = item.SubItems[4].Text.ToString().Replace("_raiz",_raiz)
                                        + "\\"
                                        + item.SubItems[0].Text.ToString()
                                        + "."
                                        + item.SubItems[2].Text.ToString()
                                        ;


                        _nombre =        item.SubItems[0].Text.ToString()
                                        + "."
                                        + item.SubItems[2].Text.ToString()
                                        ;
                        (new ViewDocumentoPDF(_ruta_completa, _nombre)).Show();

                        //MessageBox.Show(item.SubItems[3].Text.ToString()
                        //                + "\\"
                        //                + item.SubItems[0].Text.ToString()
                        //                + "."
                        //                + item.SubItems[1].Text.ToString()
                        //                );
                        //EmpIDtextBox.Text = item.SubItems[0].Text;
                        //EmpNametextBox.Text = item.SubItems[1].Text;
                    }
                }


            }
            catch (Exception)
            {

                //throw;
            }


        }// end listView1_DoubleClick

        private void button1_Click(object sender, EventArgs e)
        {
            CrearArbol();
        }

        private void ArbolDirectorio_Load(object sender, EventArgs e)
        {

        }// end load

        public void OrdenarColumna(Orden dir) {

            if (dir == Orden.Descendente)
            {
                lvwColumnSorter.Order = SortOrder.Descending;
            }
            else {
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

        }// end ordenarcolumna

        private void ListaDeArchivos_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.ListaDeArchivos.Sort();


        }// end ListaDeArchivos_ColumnClick




    }// end class
}// end namespace
