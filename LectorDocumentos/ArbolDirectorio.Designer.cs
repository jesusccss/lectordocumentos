﻿namespace LectorDocumentos
{
    partial class ArbolDirectorio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArbolDirectorio));
            this.panelDividido = new System.Windows.Forms.SplitContainer();
            this.Arbol = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ListaDeArchivos = new System.Windows.Forms.ListView();
            this.colnombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRev = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColCambio = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Colruta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Actualizar = new System.Windows.Forms.Timer(this.components);
            this.btnActualizar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelDividido)).BeginInit();
            this.panelDividido.Panel1.SuspendLayout();
            this.panelDividido.Panel2.SuspendLayout();
            this.panelDividido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelDividido
            // 
            this.panelDividido.Location = new System.Drawing.Point(12, 62);
            this.panelDividido.Name = "panelDividido";
            // 
            // panelDividido.Panel1
            // 
            this.panelDividido.Panel1.Controls.Add(this.Arbol);
            // 
            // panelDividido.Panel2
            // 
            this.panelDividido.Panel2.Controls.Add(this.ListaDeArchivos);
            this.panelDividido.Size = new System.Drawing.Size(1197, 349);
            this.panelDividido.SplitterDistance = 399;
            this.panelDividido.TabIndex = 0;
            // 
            // Arbol
            // 
            this.Arbol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Arbol.ImageIndex = 0;
            this.Arbol.ImageList = this.imageList1;
            this.Arbol.Location = new System.Drawing.Point(0, 0);
            this.Arbol.Name = "Arbol";
            this.Arbol.SelectedImageIndex = 0;
            this.Arbol.Size = new System.Drawing.Size(399, 349);
            this.Arbol.TabIndex = 0;
            this.Arbol.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ListaDeArchivos
            // 
            this.ListaDeArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colnombre,
            this.colRev,
            this.colTipo,
            this.ColCambio,
            this.Colruta});
            this.ListaDeArchivos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaDeArchivos.Location = new System.Drawing.Point(0, 0);
            this.ListaDeArchivos.Name = "ListaDeArchivos";
            this.ListaDeArchivos.Size = new System.Drawing.Size(794, 349);
            this.ListaDeArchivos.SmallImageList = this.imageList1;
            this.ListaDeArchivos.TabIndex = 0;
            this.ListaDeArchivos.UseCompatibleStateImageBehavior = false;
            this.ListaDeArchivos.View = System.Windows.Forms.View.Details;
            this.ListaDeArchivos.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListaDeArchivos_ColumnClick);
            this.ListaDeArchivos.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView1_ItemSelectionChanged);
            this.ListaDeArchivos.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.ListaDeArchivos.Click += new System.EventHandler(this.listView1_Click);
            this.ListaDeArchivos.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // colnombre
            // 
            this.colnombre.Text = "Nombre";
            this.colnombre.Width = 400;
            // 
            // colRev
            // 
            this.colRev.Text = "Rev";
            // 
            // colTipo
            // 
            this.colTipo.Text = "Tipo";
            this.colTipo.Width = 200;
            // 
            // ColCambio
            // 
            this.ColCambio.Text = "Ultimo Cambio";
            this.ColCambio.Width = 200;
            // 
            // Colruta
            // 
            this.Colruta.Text = "Directorio";
            // 
            // Actualizar
            // 
            this.Actualizar.Interval = 5000;
            this.Actualizar.Tick += new System.EventHandler(this.Actualizar_Tick);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(1067, 12);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(105, 23);
            this.btnActualizar.TabIndex = 1;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LectorDocumentos.Properties.Resources.TERMOTECNICA;
            this.pictureBox1.Location = new System.Drawing.Point(12, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // ArbolDirectorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 437);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.panelDividido);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ArbolDirectorio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lector de documentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ArbolDirectorio_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panelDividido.Panel1.ResumeLayout(false);
            this.panelDividido.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelDividido)).EndInit();
            this.panelDividido.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer panelDividido;
        private System.Windows.Forms.TreeView Arbol;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListView ListaDeArchivos;
        private System.Windows.Forms.ColumnHeader colnombre;
        private System.Windows.Forms.ColumnHeader colTipo;
        private System.Windows.Forms.ColumnHeader ColCambio;
        private System.Windows.Forms.Timer Actualizar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.ColumnHeader Colruta;
        private System.Windows.Forms.ColumnHeader colRev;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

