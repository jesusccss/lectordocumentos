﻿namespace LectorDocumentos
{
    partial class ArbolDirectorio02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArbolDirectorio02));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnActualizar = new DevExpress.XtraBars.BarButtonItem();
            this.panelDocumentos = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.panelDividido = new System.Windows.Forms.SplitContainer();
            this.Arbol = new System.Windows.Forms.TreeView();
            this.ListaDeArchivos = new System.Windows.Forms.ListView();
            this.colnombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRev = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColCambio = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Colruta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblVersion = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelDividido)).BeginInit();
            this.panelDividido.Panel1.SuspendLayout();
            this.panelDividido.Panel2.SuspendLayout();
            this.panelDividido.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnActualizar,
            this.lblVersion});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 3;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.panelDocumentos});
            this.ribbon.Size = new System.Drawing.Size(972, 143);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Click += new System.EventHandler(this.ribbon_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Caption = "Actualizar";
            this.btnActualizar.Glyph = ((System.Drawing.Image)(resources.GetObject("btnActualizar.Glyph")));
            this.btnActualizar.Id = 1;
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnActualizar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnActualizar_ItemClick);
            // 
            // panelDocumentos
            // 
            this.panelDocumentos.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.panelDocumentos.Name = "panelDocumentos";
            this.panelDocumentos.Text = "Documentos";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnActualizar);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Inicio";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.lblVersion);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 619);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(972, 31);
            // 
            // panelDividido
            // 
            this.panelDividido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDividido.Location = new System.Drawing.Point(0, 143);
            this.panelDividido.Name = "panelDividido";
            // 
            // panelDividido.Panel1
            // 
            this.panelDividido.Panel1.Controls.Add(this.Arbol);
            // 
            // panelDividido.Panel2
            // 
            this.panelDividido.Panel2.Controls.Add(this.ListaDeArchivos);
            this.panelDividido.Size = new System.Drawing.Size(972, 476);
            this.panelDividido.SplitterDistance = 324;
            this.panelDividido.TabIndex = 2;
            // 
            // Arbol
            // 
            this.Arbol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Arbol.Location = new System.Drawing.Point(0, 0);
            this.Arbol.Name = "Arbol";
            this.Arbol.Size = new System.Drawing.Size(324, 476);
            this.Arbol.TabIndex = 0;
            this.Arbol.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Arbol_AfterSelect);
            this.Arbol.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.Arbol_NodeMouseClick);
            this.Arbol.Click += new System.EventHandler(this.Arbol_Click);
            // 
            // ListaDeArchivos
            // 
            this.ListaDeArchivos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colnombre,
            this.colRev,
            this.colTipo,
            this.ColCambio,
            this.Colruta});
            this.ListaDeArchivos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaDeArchivos.Location = new System.Drawing.Point(0, 0);
            this.ListaDeArchivos.Name = "ListaDeArchivos";
            this.ListaDeArchivos.Size = new System.Drawing.Size(644, 476);
            this.ListaDeArchivos.TabIndex = 0;
            this.ListaDeArchivos.UseCompatibleStateImageBehavior = false;
            this.ListaDeArchivos.View = System.Windows.Forms.View.Details;
            this.ListaDeArchivos.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListaDeArchivos_ColumnClick);
            this.ListaDeArchivos.DoubleClick += new System.EventHandler(this.ListaDeArchivos_DoubleClick);
            // 
            // colnombre
            // 
            this.colnombre.Text = "Nombre";
            this.colnombre.Width = 400;
            // 
            // colRev
            // 
            this.colRev.Text = "Rev";
            // 
            // colTipo
            // 
            this.colTipo.Text = "Tipo";
            this.colTipo.Width = 200;
            // 
            // ColCambio
            // 
            this.ColCambio.Text = "Ultimo Cambio";
            this.ColCambio.Width = 200;
            // 
            // Colruta
            // 
            this.Colruta.Text = "Directorio";
            // 
            // lblVersion
            // 
            this.lblVersion.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblVersion.Caption = "barStaticItem1";
            this.lblVersion.Id = 2;
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ArbolDirectorio02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 650);
            this.Controls.Add(this.panelDividido);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ArbolDirectorio02";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Lector de documentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ArbolDirectorio02_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            this.panelDividido.Panel1.ResumeLayout(false);
            this.panelDividido.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelDividido)).EndInit();
            this.panelDividido.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage panelDocumentos;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnActualizar;
        private System.Windows.Forms.SplitContainer panelDividido;
        private System.Windows.Forms.TreeView Arbol;
        private System.Windows.Forms.ListView ListaDeArchivos;
        private System.Windows.Forms.ColumnHeader colnombre;
        private System.Windows.Forms.ColumnHeader colRev;
        private System.Windows.Forms.ColumnHeader colTipo;
        private System.Windows.Forms.ColumnHeader ColCambio;
        private System.Windows.Forms.ColumnHeader Colruta;
        private DevExpress.XtraBars.BarStaticItem lblVersion;
    }
}