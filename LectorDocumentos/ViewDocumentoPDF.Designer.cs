﻿namespace LectorDocumentos
{
    partial class ViewDocumentoPDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewDocumentoPDF));
            this.pdfViewer01 = new DevExpress.XtraPdfViewer.PdfViewer();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.visualizador = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.SuspendLayout();
            // 
            // pdfViewer01
            // 
            this.pdfViewer01.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pdfViewer01.ContentMarginMode = DevExpress.XtraPdfViewer.PdfContentMarginMode.Static;
            this.pdfViewer01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfViewer01.Location = new System.Drawing.Point(0, 0);
            this.pdfViewer01.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pdfViewer01.Name = "pdfViewer01";
            this.pdfViewer01.Size = new System.Drawing.Size(1003, 728);
            this.pdfViewer01.TabIndex = 1;
            this.pdfViewer01.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pdfViewer01_MouseDown);
            this.pdfViewer01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pdfViewer01_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.DropShadowEnabled = false;
            this.contextMenuStrip1.Enabled = false;
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.ShowItemToolTips = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(36, 4);
            // 
            // visualizador
            // 
            this.visualizador.IsMetric = true;
            this.visualizador.Location = new System.Drawing.Point(782, 77);
            this.visualizador.Name = "visualizador";
            this.visualizador.Size = new System.Drawing.Size(209, 122);
            this.visualizador.TabIndex = 0;
            this.visualizador.Visible = false;
            // 
            // ViewDocumentoPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 728);
            this.Controls.Add(this.pdfViewer01);
            this.Controls.Add(this.visualizador);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ViewDocumentoPDF";
            this.Text = "Documento";
            this.Load += new System.EventHandler(this.ViewDocumentoPDF_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraPdfViewer.PdfViewer pdfViewer01;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraPrinting.Preview.DocumentViewer visualizador;
    }
}