﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
//using gnu.javax.crypto.assembly;
//using System.Reflection;

namespace LectorDocumentos
{
    public partial class ArbolDirectorio02 : DevExpress.XtraBars.Ribbon.RibbonForm{
        public ArbolDirectorio02(){
            InitializeComponent();


            CrearArbol();
            colocar();

            // Create an instance of a ListView column sorter and assign it // to the ListView control.
            Columna_Ordenar = new ListViewColumnSorter();
            this.ListaDeArchivos.ListViewItemSorter = Columna_Ordenar;
            OrdenarColumna(Orden.Ascendente);// ordeno la columna


        }// end inicializar

        public enum Orden { Ascendente = 0, Descendente = 1 };
        private ListViewColumnSorter Columna_Ordenar;// columna para ordenador

        public string _raiz = @"\\10.130.233.25\compartida\DocumentosCertificacion";
        //public string _raiz = @"\\10.130.233.175\ComparDevJesus\DOC";
        private string referenciaLetras = "abcdefghijklmnopqrstuvwxyz1234567890 +-*/._:;,<>()°|!";

        private string _version_app = "1.0.0.6.A02";

        private void CrearArbol()
        {
            Arbol.Nodes.Clear();// elimina nodos
            //ListaDeArchivos.Clear();
            TreeNode rootNode;

            //            DirectoryInfo info = new DirectoryInfo(@"../..");
            DirectoryInfo info = new DirectoryInfo(_raiz);
            if (info.Exists)
            {
                rootNode = new TreeNode(info.Name);
                rootNode.Tag = info;
                GetDirectories(info.GetDirectories(), rootNode);
                Arbol.Nodes.Add(rootNode);
            }
            Arbol.Sort();// ordenar arbol
            Arbol.ExpandAll(); // expandir arbol
        }// end PopulateTreeView

        private void GetDirectories(DirectoryInfo[] subDirs, TreeNode nodeToAddTo)
        {
            TreeNode aNode;
            DirectoryInfo[] subSubDirs;
            foreach (DirectoryInfo subDir in subDirs)
            {
                aNode = new TreeNode(subDir.Name, 0, 0);
                aNode.Tag = subDir;
                aNode.ImageKey = "folder";
                subSubDirs = subDir.GetDirectories();
                if (subSubDirs.Length != 0)
                {
                    GetDirectories(subSubDirs, aNode);
                }
                nodeToAddTo.Nodes.Add(aNode);
            }
        }// end GetDirectories


        private void ArbolDirectorio02_Load(object sender, EventArgs e){


            //lblVersion.Caption = Application.ProductVersion.ToString();
            lblVersion.Caption = _version_app.ToString();
        }// end load

        private void Arbol_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void Arbol_Click(object sender, EventArgs e)
        {

        }

        private void Arbol_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode _Nodo_seleccionado = e.Node;
            ListaDeArchivos.Items.Clear();
            DirectoryInfo _Nodo_informacion = (DirectoryInfo)_Nodo_seleccionado.Tag;
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;

            string nombre_archivo = "", extencion = "", _revision = "";
            ////char punto = '.';
            //ListBox listaDesorden = new ListBox();
            //foreach (FileInfo file in _Nodo_informacion.GetFiles())
            //{
            //    listaDesorden.Items.Add(file.Name);
            //}// 

            //for (int i = 0; i < listaDesorden.Items.Count; i++)
            //{



            //}// ordenar lista


            string REV = "",_ext_temporal="";
            //string nombreTemporal = "";
            string Ref = "1234567890";
            string[] _extencion= { "",""};/*me sirve para obtener la extencion*/
            foreach (FileInfo file in _Nodo_informacion.GetFiles())
            {
                nombre_archivo = file.Name.Substring(0, file.Name.Length - 4);
                _extencion = file.Name.Split('.');/*separo el nombre por punto*/
                _ext_temporal = _extencion[_extencion.Length - 1];/*obtengo temporalmente la extencion*/
                if (_extencion.Length > 1){/*por alguno caso que no tenga extencion, asigno una por default*/
                    if (   (_ext_temporal.ToLower() == "pdf")
                        //|| (_ext_temporal.ToLower() == "docx")
                        //|| (_ext_temporal.ToLower() == "doc")
                        //|| (_ext_temporal.ToLower() == "xls")
                        //|| (_ext_temporal.ToLower() == "txt")
                        ) {
                        extencion = _ext_temporal;/*asigno el ultimo elemento*/
                        //MessageBox.Show("permitido: "+extencion);
                    }
                    else { extencion = "XXX";
                        //MessageBox.Show("No esta en las condiciones: " + extencion);
                    }
                }
                else{
                    extencion = "XXX";
                    //MessageBox.Show("elementos: " + _extencion.Length);
                }

                //MessageBox.Show(
                //      "Archivo: " + file.Name
                //    + Environment.NewLine
                //    + "Extencion: " + extencion
                //    + Environment.NewLine
                //    + "Valor: " + _extencion[_extencion.Length - 1]
                //    );


                //extencion = file.Name.Substring(file.Name.Length - 3, 3);
                try
                {
                    if (file.Name.Contains("REV.")){
                        REV = file.Name.Substring(file.Name.IndexOf("REV.") + 4, 2);
                        if ((Ref.Contains(REV.Substring(0, 1)))
                                &&
                                ((Ref + " ").Contains(REV.Substring(1, 1)))
                           )
                        {
                            _revision = "Rev." + REV;/* extraer version del documento*/
                        }
                        else { _revision = "Rev.XX"; }
                    }else { _revision = "Rev.XX"; }

                }catch (Exception){
                    _revision = "Rev.XX";
                }
                //MessageBox.Show();
                //file.Name.IndexOf("REV.")
                //if (file.Name.Contains("REV.")) {

                //    nombreTemporal = file.Name.Replace(" ", "_");

                //    REV = nombreTemporal.Replace("REV."," ");

                //    _revision = "Rev.00";

                //}else {
                //    _revision = "Rev.XX";
                //}

                //if (file.Name.Contains("REV.0")) { _revision = "Rev.00"; }

                //else if (file.Name.Contains("REV.00")) { _revision = "Rev.00"; }

                //else if (file.Name.Contains("REV.01")) { _revision = "Rev.01"; }
                //else if (file.Name.Contains("REV.02")) { _revision = "Rev.02"; }
                //else if (file.Name.Contains("REV.03")) { _revision = "Rev.03"; }
                //else if (file.Name.Contains("REV.04")) { _revision = "Rev.04"; }
                //else if (file.Name.Contains("REV.05")) { _revision = "Rev.05"; }
                //else if (file.Name.Contains("REV.06")) { _revision = "Rev.06"; }
                //else if (file.Name.Contains("REV.07")) { _revision = "Rev.07"; }
                //else if (file.Name.Contains("REV.08")) { _revision = "Rev.08"; }
                //else if (file.Name.Contains("REV.09")) { _revision = "Rev.09"; }
                //else if (file.Name.Contains("REV.10")) { _revision = "Rev.10"; }

                //else if (file.Name.Contains("REV.11")) { _revision = "Rev.11"; }
                //else if (file.Name.Contains("REV.12")) { _revision = "Rev.12"; }
                //else if (file.Name.Contains("REV.13")) { _revision = "Rev.13"; }
                //else if (file.Name.Contains("REV.14")) { _revision = "Rev.14"; }
                //else if (file.Name.Contains("REV.15")) { _revision = "Rev.15"; }
                //else if (file.Name.Contains("REV.16")) { _revision = "Rev.16"; }
                //else if (file.Name.Contains("REV.17")) { _revision = "Rev.17"; }
                //else if (file.Name.Contains("REV.18")) { _revision = "Rev.18"; }
                //else if (file.Name.Contains("REV.19")) { _revision = "Rev.19"; }
                //else if (file.Name.Contains("REV.20")) { _revision = "Rev.20"; }

                //else if (file.Name.Contains("REV.21")) { _revision = "Rev.21"; }
                //else if (file.Name.Contains("REV.22")) { _revision = "Rev.22"; }
                //else if (file.Name.Contains("REV.23")) { _revision = "Rev.23"; }
                //else if (file.Name.Contains("REV.24")) { _revision = "Rev.24"; }
                //else if (file.Name.Contains("REV.25")) { _revision = "Rev.25"; }
                //else if (file.Name.Contains("REV.26")) { _revision = "Rev.26"; }
                //else if (file.Name.Contains("REV.27")) { _revision = "Rev.27"; }
                //else if (file.Name.Contains("REV.28")) { _revision = "Rev.28"; }
                //else if (file.Name.Contains("REV.29")) { _revision = "Rev.29"; }
                //else if (file.Name.Contains("REV.30")) { _revision = "Rev.30"; }

                //else if (file.Name.Contains("REV.31")) { _revision = "Rev.31"; }
                //else if (file.Name.Contains("REV.32")) { _revision = "Rev.32"; }
                //else if (file.Name.Contains("REV.33")) { _revision = "Rev.33"; }
                //else if (file.Name.Contains("REV.34")) { _revision = "Rev.34"; }
                //else if (file.Name.Contains("REV.35")) { _revision = "Rev.35"; }
                //else if (file.Name.Contains("REV.36")) { _revision = "Rev.36"; }
                //else if (file.Name.Contains("REV.37")) { _revision = "Rev.37"; }
                //else if (file.Name.Contains("REV.38")) { _revision = "Rev.38"; }
                //else if (file.Name.Contains("REV.39")) { _revision = "Rev.39"; }
                //else if (file.Name.Contains("REV.40")) { _revision = "Rev.40"; }

                //else if (file.Name.Contains("REV.41")) { _revision = "Rev.41"; }
                //else if (file.Name.Contains("REV.42")) { _revision = "Rev.42"; }
                //else if (file.Name.Contains("REV.43")) { _revision = "Rev.43"; }
                //else if (file.Name.Contains("REV.44")) { _revision = "Rev.44"; }
                //else if (file.Name.Contains("REV.45")) { _revision = "Rev.45"; }
                //else if (file.Name.Contains("REV.46")) { _revision = "Rev.46"; }
                //else if (file.Name.Contains("REV.47")) { _revision = "Rev.47"; }
                //else if (file.Name.Contains("REV.48")) { _revision = "Rev.48"; }
                //else if (file.Name.Contains("REV.49")) { _revision = "Rev.49"; }
                //else if (file.Name.Contains("REV.50")) { _revision = "Rev.50"; }

                //else if (file.Name.Contains("REV.51")) { _revision = "Rev.51"; }
                //else if (file.Name.Contains("REV.52")) { _revision = "Rev.52"; }
                //else if (file.Name.Contains("REV.53")) { _revision = "Rev.53"; }
                //else if (file.Name.Contains("REV.54")) { _revision = "Rev.54"; }
                //else if (file.Name.Contains("REV.55")) { _revision = "Rev.55"; }
                //else if (file.Name.Contains("REV.56")) { _revision = "Rev.56"; }
                //else if (file.Name.Contains("REV.57")) { _revision = "Rev.57"; }
                //else if (file.Name.Contains("REV.58")) { _revision = "Rev.58"; }
                //else if (file.Name.Contains("REV.59")) { _revision = "Rev.59"; }
                //else if (file.Name.Contains("REV.60")) { _revision = "Rev.60"; }

                //else if (file.Name.Contains("REV.61")) { _revision = "Rev.61"; }
                //else if (file.Name.Contains("REV.62")) { _revision = "Rev.62"; }
                //else if (file.Name.Contains("REV.63")) { _revision = "Rev.63"; }
                //else if (file.Name.Contains("REV.64")) { _revision = "Rev.64"; }
                //else if (file.Name.Contains("REV.65")) { _revision = "Rev.65"; }
                //else if (file.Name.Contains("REV.66")) { _revision = "Rev.66"; }
                //else if (file.Name.Contains("REV.67")) { _revision = "Rev.67"; }
                //else if (file.Name.Contains("REV.68")) { _revision = "Rev.68"; }
                //else if (file.Name.Contains("REV.69")) { _revision = "Rev.69"; }
                //else if (file.Name.Contains("REV.70")) { _revision = "Rev.70"; }
                                                 
                //else if (file.Name.Contains("REV.71")) { _revision = "Rev.71"; }
                //else if (file.Name.Contains("REV.72")) { _revision = "Rev.72"; }
                //else if (file.Name.Contains("REV.73")) { _revision = "Rev.73"; }
                //else if (file.Name.Contains("REV.74")) { _revision = "Rev.74"; }
                //else if (file.Name.Contains("REV.75")) { _revision = "Rev.75"; }
                //else if (file.Name.Contains("REV.76")) { _revision = "Rev.76"; }
                //else if (file.Name.Contains("REV.77")) { _revision = "Rev.77"; }
                //else if (file.Name.Contains("REV.78")) { _revision = "Rev.78"; }
                //else if (file.Name.Contains("REV.79")) { _revision = "Rev.79"; }
                //else if (file.Name.Contains("REV.80")) { _revision = "Rev.80"; }
                                                 
                //else if (file.Name.Contains("REV.81")) { _revision = "Rev.81"; }
                //else if (file.Name.Contains("REV.82")) { _revision = "Rev.82"; }
                //else if (file.Name.Contains("REV.83")) { _revision = "Rev.83"; }
                //else if (file.Name.Contains("REV.84")) { _revision = "Rev.84"; }
                //else if (file.Name.Contains("REV.85")) { _revision = "Rev.85"; }
                //else if (file.Name.Contains("REV.86")) { _revision = "Rev.86"; }
                //else if (file.Name.Contains("REV.87")) { _revision = "Rev.87"; }
                //else if (file.Name.Contains("REV.88")) { _revision = "Rev.88"; }
                //else if (file.Name.Contains("REV.89")) { _revision = "Rev.89"; }
                //else if (file.Name.Contains("REV.90")) { _revision = "Rev.90"; }
                                                 
                //else if (file.Name.Contains("REV.91")) { _revision = "Rev.91"; }
                //else if (file.Name.Contains("REV.92")) { _revision = "Rev.92"; }
                //else if (file.Name.Contains("REV.93")) { _revision = "Rev.93"; }
                //else if (file.Name.Contains("REV.94")) { _revision = "Rev.94"; }
                //else if (file.Name.Contains("REV.95")) { _revision = "Rev.95"; }
                //else if (file.Name.Contains("REV.96")) { _revision = "Rev.96"; }
                //else if (file.Name.Contains("REV.97")) { _revision = "Rev.97"; }
                //else if (file.Name.Contains("REV.98")) { _revision = "Rev.98"; }
                //else if (file.Name.Contains("REV.99")) { _revision = "Rev.99"; }
                //else if (file.Name.Contains("REV.100")) { _revision = "Rev.100"; }

                //else { _revision = "Rev.XX"; }


                if (extencion != "XXX")
                {// solo pdf
                    item = new ListViewItem(nombre_archivo, 1);// nombre
                    subItems = new ListViewItem.ListViewSubItem[]// agregar datos
                              {    new ListViewItem.ListViewSubItem(item, _revision)// rev
                                  ,new ListViewItem.ListViewSubItem(item, extencion)// extencion
                                  ,new ListViewItem.ListViewSubItem(item,file.LastAccessTime.ToShortDateString())//ultima modificacion
                                  ,new ListViewItem.ListViewSubItem(item,file.DirectoryName.ToString().Replace(_raiz,"_raiz"))//directorio
                              };

                    item.SubItems.AddRange(subItems);
                    ListaDeArchivos.Items.Add(item);

                }// end if solo pdf
            }

            ListaDeArchivos.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }


        public void colocar()
        {

            //panelDividido.Top = 30;
            //panelDividido.Height = this.Height - panelDividido.Top;

            //panelDividido.Left = 0;
            //panelDividido.Width = this.Width - 16;



        }// end colocar

        private void ListaDeArchivos_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                if (ListaDeArchivos.SelectedItems.Count > 0)
                {


                    ListViewItem item = ListaDeArchivos.SelectedItems[0];
                    string _ruta_completa = "", _nombre;
                    if (item != null)
                    {


                        _ruta_completa = item.SubItems[4].Text.ToString().Replace("_raiz", _raiz)
                                        + "\\"
                                        + item.SubItems[0].Text.ToString()
                                        + "."
                                        + item.SubItems[2].Text.ToString()
                                        ;


                        _nombre = item.SubItems[0].Text.ToString()
                                        + "."
                                        + item.SubItems[2].Text.ToString()
                                        ;
                        (new ViewDocumentoPDF(_ruta_completa, _nombre)).Show();
                    }
                }


            }
            catch (Exception)
            {

                //throw;
            }

        }

        private void btnActualizar_ItemClick(object sender, ItemClickEventArgs e)
        {
            CrearArbol();
            //Arbol.MouseClick(sender);
            //Arbol_NodeMouseClick(sender,TreeNode. );
            
        }



        public void OrdenarColumna(Orden dir)
        {

            if (dir == Orden.Descendente)
            {
                Columna_Ordenar.Order = SortOrder.Descending;
            }
            else
            {
                Columna_Ordenar.Order = SortOrder.Ascending;
            }

        }// end ordenarcolumna

        private void ListaDeArchivos_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == Columna_Ordenar.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (Columna_Ordenar.Order == SortOrder.Ascending)
                {
                    Columna_Ordenar.Order = SortOrder.Descending;
                }
                else
                {
                    Columna_Ordenar.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                Columna_Ordenar.SortColumn = e.Column;
                Columna_Ordenar.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.ListaDeArchivos.Sort();


        }// end ListaDeArchivos_ColumnClick

        private void ribbon_Click(object sender, EventArgs e)
        {

        }


    }// end class
}// end namespace